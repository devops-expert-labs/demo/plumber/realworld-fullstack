package com.example.realworld.domain.aggregate.user.service;

import com.example.realworld.domain.aggregate.user.dto.UserAuth;
import com.example.realworld.domain.aggregate.user.dto.UserResponse;
import com.example.realworld.domain.aggregate.user.dto.UserSigninRequest;
import com.example.realworld.domain.aggregate.user.dto.UserSignupRequest;
import com.example.realworld.domain.aggregate.user.dto.UserUpdate;

public interface UserService {
    UserResponse signup(UserSignupRequest userSignupRequest);

    UserResponse signin(UserSigninRequest userSigninRequest);

    UserResponse getCurrentUser(UserAuth userAuth);

    UserResponse updateUser(UserUpdate userUpdate, UserAuth userAuth);
}
