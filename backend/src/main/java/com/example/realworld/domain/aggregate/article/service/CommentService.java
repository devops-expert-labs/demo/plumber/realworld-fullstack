package com.example.realworld.domain.aggregate.article.service;

import com.example.realworld.domain.aggregate.article.dto.CommentResponse;
import com.example.realworld.domain.aggregate.article.dto.Commentdto;
import com.example.realworld.domain.aggregate.user.dto.UserAuth;

import java.util.List;

public interface CommentService {

    List<CommentResponse> getComments(UserAuth userAuth, String slug);

    CommentResponse addComment(UserAuth userAuth, String slug, Commentdto commentdto);

    void deleteComment(UserAuth userAuth, String slug, Long id);
}
