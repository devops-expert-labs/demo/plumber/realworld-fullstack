package com.example.realworld.domain.aggregate.user.controller;

import com.example.realworld.domain.aggregate.user.dto.UserResponse;
import com.example.realworld.domain.aggregate.user.dto.UserSigninRequest;
import com.example.realworld.domain.aggregate.user.dto.UserSignupRequest;
import com.example.realworld.domain.aggregate.user.service.UserServiceImpl;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/users")
public class UsersController {
    private final UserServiceImpl userService;

    public UsersController(UserServiceImpl userService) {
        this.userService = userService;
    }

    @PostMapping
    public UserResponse signup(@Valid @RequestBody UserSignupRequest userSignupRequest) {
        return userService.signup(userSignupRequest);
    }

    @PostMapping(value = "/login")
    public UserResponse signin(@Valid @RequestBody UserSigninRequest userSigninRequest) {
        return userService.signin(userSigninRequest);
    }
}
