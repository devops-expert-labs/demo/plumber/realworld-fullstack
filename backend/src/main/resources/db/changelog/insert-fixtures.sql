------------------------------------------------------------
-- Inserts fixtures for development and testing purposes
------------------------------------------------------------

--------------------
-- Insert Users
--------------------
INSERT INTO "user" (id, username, email, password, image)
VALUES 
  (11, 'johndoe', 'johndoe@example.com', '$2a$10$6l4PTzkA0t9PvRGzCSKO6exOSZnfcMnU86G4zoVL9rIrVjWfGvTyW', 'https://fastly.picsum.photos/id/870/128/128.jpg?hmac=2ZV4rAlk84KkKihP8rQmhwGnLuz4XHQDNqTptS93gsg'),
  (12, 'janedoe', 'janedoe@example.com', '$2a$10$6l4PTzkA0t9PvRGzCSKO6exOSZnfcMnU86G4zoVL9rIrVjWfGvTyW', 'https://fastly.picsum.photos/id/348/128/128.jpg?hmac=rVEm96iU_fNyKji6ZuQGe6cjZjp6xGMnY9ganlJebew'),
  (13, 'bobsmith', 'bobsmith@example.com', '$2a$10$6l4PTzkA0t9PvRGzCSKO6exOSZnfcMnU86G4zoVL9rIrVjWfGvTyW', 'https://fastly.picsum.photos/id/662/128/128.jpg?hmac=QVOT0StEK4Toi7b2WqqTQihE2XKic3foUjqm2YyggDc');

--------------------
-- Insert Articles
--------------------
INSERT INTO "article" (id, title, description, body, author_id, slug, created_at, updated_at)
VALUES 
  -- Articles for user with ID 11
  (11, '10 Tips for Successful Time Management', 'Learn how to manage your time more effectively and achieve your goals', 'Effective time management is essential for achieving success in both your personal and professional life. In this article, we will share ten tips for successful time management and provide practical strategies for incorporating them into your daily routine.', 11, '10-tips-for-successful-time-management-11', '2022-01-01 10:00:00', '2022-01-01 10:00:00'),
  (12, 'The Power of Positive Thinking', 'Discover the benefits of positive thinking and learn how to cultivate a positive mindset', 'Positive thinking can help you overcome challenges, reduce stress, and improve your overall well-being. In this article, we will explore the power of positive thinking and provide practical tips and techniques for cultivating a more positive mindset.', 11, 'the-power-of-positive-thinking-12', '2022-02-01 15:30:00', '2022-02-01 15:30:00'),
  (13, 'The Benefits of Mindfulness Meditation', 'Learn how to reduce stress and increase focus with mindfulness meditation', 'Mindfulness meditation is a powerful practice that can help you reduce stress, increase focus, and cultivate a greater sense of well-being. In this article, we will explore the benefits of mindfulness meditation and provide practical tips and techniques for incorporating it into your daily routine.', 11, 'the-benefits-of-mindfulness-meditation-13', '2022-03-01 08:45:00', '2022-03-01 08:45:00'),
  (14, 'How to Overcome Procrastination', 'Learn how to stop procrastinating and get things done', 'Procrastination can be a major obstacle to achieving your goals. In this article, we will explore the causes of procrastination and provide practical tips and techniques for overcoming it and getting things done.', 11, 'how-to-overcome-procrastination-14', '2022-04-01 12:15:00', '2022-04-01 12:15:00'),
  (15, 'The Benefits of Regular Exercise', 'Discover the physical and mental benefits of regular exercise', 'Regular exercise is essential for maintaining good physical and mental health. In this article, we will explore the many benefits of exercise and provide practical tips and techniques for incorporating it into your daily routine.', 11, 'the-benefits-of-regular-exercise-15', '2022-05-01 17:00:00', '2022-05-01 17:00:00'),

  -- Articles for user with ID 12
  (16, 'The Benefits of Yoga', 'Discover the physical and mental benefits of practicing yoga', 'Yoga is a practice that can help you reduce stress, increase flexibility, and cultivate a greater sense of well-being. In this article, we will explore the benefits of yoga and provide practical tips and techniques for incorporating it into your daily routine.', 12, 'the-benefits-of-yoga-16', '2022-01-01 10:00:00', '2022-01-01 10:00:00'),
  (17, 'The Power of Gratitude', 'Discover the benefits of practicing gratitude and learn how to cultivate it in your daily life', 'Gratitude is a powerful emotion that can help you reduce stress, increase happiness, and cultivate a more positive outlook on life. In this article, we will explore the power of gratitude and provide practical tips and techniques for incorporating it into your daily routine.', 12, 'the-power-of-gratitude-17', '2022-02-01 15:30:00', '2022-02-01 15:30:00'),
  (18, 'The Benefits of Mindful Eating', 'Learn how to improve your relationship with food and cultivate healthier eating habits', 'Mindful eating is a practice that can help you become more aware of your food choices and develop a healthier relationship with food. In this article, we will explore the benefits of mindful eating and provide practical tips and techniques for incorporating it into your daily routine.', 12, 'the-benefits-of-mindful-eating-18', '2022-03-01 08:45:00', '2022-03-01 08:45:00'),
  (19, 'The Art of Journaling', 'Learn how to reflect on your experiences and gain insights about yourself through journaling', 'Journaling is a powerful tool that can help you gain insights about yourself, your values, and your goals. In this article, we will explore the art of journaling and provide practical tips and techniques for incorporating it into your daily routine.', 12, 'the-art-of-journaling-19', '2022-04-01 12:15:00', '2022-04-01 12:15:00'),
  (20, 'The Benefits of Gratitude Journaling', 'Learn how to cultivate a sense of gratitude and improve your well-being through journaling', 'Gratitude journaling is a practice that can help you become more aware of the things you are thankful for and cultivate a greater sense of well-being. In this article, we will explore the benefits of gratitude journaling and provide practical tips and techniques for incorporating it into your daily routine.', 12, 'the-benefits-of-gratitude-journaling-20', '2022-05-01 17:00:00', '2022-05-01 17:00:00'),

  -- Articles for user with ID 13
  (21, 'The Benefits of Deep Breathing', 'Learn how to reduce stress and increase relaxation with deep breathing exercises', 'Deep breathing is a simple yet powerful technique that can help you reduce stress, increase relaxation, and improve your overall well-being. In this article, we will explore the benefits of deep breathing and provide practical tips and techniques for incorporating it into your daily routine.', 13, 'the-benefits-of-deep-breathing-21', '2022-01-01 10:00:00', '2022-01-01 10:00:00'),
  (22, 'The Power of Visualization', 'Learn how to use visualization techniques to achieve your goals and overcome obstacles', 'Visualization is a powerful technique that can help you achieve your goals, overcome obstacles, and cultivate a more positive mindset. In this article, we will explore the power of visualization and provide practical tips and techniques for incorporating it into your daily routine.', 13, 'the-power-of-visualization-22', '2022-02-01 15:30:00', '2022-02-01 15:30:00'),
  (23, 'The Benefits of a Gratitude Practice', 'Learn how to cultivate gratitude and improve your well-being through a daily practice', 'Cultivating gratitude is a powerful practice that can help you reduce stress, increase happiness, and improve your overall well-being. In this article, we will explore the benefits of a gratitude practice and provide practical tips and techniques for incorporating it into your daily routine.', 13, 'the-benefits-of-a-gratitude-practice-23', '2022-03-01 08:45:00', '2022-03-01 08:45:00'),
  (24, 'The Power of Affirmations', 'Learn how to use affirmations to cultivate a more positive mindset and achieve your goals', 'Affirmations are positive statements that can help you overcome negative thinking patterns, cultivate a more positive mindset, and achieve your goals. In this article, we will explore the power of affirmations and provide practical tips and techniques for incorporating them into your daily routine.', 13, 'the-power-of-affirmations-24', '2022-04-01 12:15:00', '2022-04-01 12:15:00'),
  (25, 'The Benefits of a Daily Gratitude Journal', 'Learn how to cultivate gratitude and improve your well-being through a daily journaling practice', 'Keeping a daily gratitude journal is a powerful practice that can help you become more aware of the things you are thankful for and cultivate a greater sense of well-being. In this article, we will explore the benefits of a daily gratitude journal and provide practical tips and techniques for incorporating it into your daily routine.', 13, 'the-benefits-of-a-daily-gratitude-journal-25', '2022-05-01 17:00:00', '2022-05-01 17:00:00');

--------------------
-- Insert Comments
--------------------
INSERT INTO "comment" (body, created_at, updated_at, author_id, article_id)
VALUES
  ('Great article, I learned a lot from this!', '2022-05-01 10:00:00', '2022-05-01 10:00:00', 11, 11),
  ('I found this article to be very informative, thanks for sharing!', '2022-05-02 12:30:00', '2022-05-02 12:30:00', 12, 11),
  ('This article was a real eye-opener for me, I never realized how important this topic was!', '2022-05-03 15:45:00', '2022-05-03 15:45:00', 13, 11),
  
  ('I really enjoyed reading this article, it was very well-written and easy to follow', '2022-05-04 09:15:00', '2022-05-04 09:15:00', 11, 12),
  ('This article gave me some great ideas, I can not wait to try them out!', '2022-05-05 11:30:00', '2022-05-05 11:30:00', 12, 12),
  ('Thanks for sharing your insights on this topic, I found them very helpful', '2022-05-06 14:45:00', '2022-05-06 14:45:00', 13, 12),
  
  ('This article was really interesting, I had never thought about this topic before', '2022-05-07 08:00:00', '2022-05-07 08:00:00', 11, 13),
  ('I appreciate the practical tips and advice you provided in this article', '2022-05-08 10:30:00', '2022-05-08 10:30:00', 12, 13),
  ('Great article, I am looking forward to incorporating some of these ideas into my daily routine', '2022-05-09 13:45:00', '2022-05-09 13:45:00', 13, 13),
  
  ('This article was really informative, I learned a lot from it', '2022-05-10 07:15:00', '2022-05-10 07:15:00', 11, 14),
  ('Thanks for sharing your insights on this topic, I found them very helpful', '2022-05-11 09:30:00', '2022-05-11 09:30:00', 12, 14),
  ('This article gave me some great ideas, I can not wait to try them out!', '2022-05-12 12:45:00', '2022-05-12 12:45:00', 13, 14),
  
  ('Great article, I learned a lot from this!', '2022-05-01 10:00:00', '2022-05-01 10:00:00', 11, 15),
  ('I found this article to be very informative, thanks for sharing!', '2022-05-02 12:30:00', '2022-05-02 12:30:00', 12, 15),
  ('This article was a real eye-opener for me, I never realized how important this topic was!', '2022-05-03 15:45:00', '2022-05-03 15:45:00', 13, 15),
  
  ('I really enjoyed reading this article, it was very well-written and easy to follow', '2022-05-04 09:15:00', '2022-05-04 09:15:00', 11, 16),
  ('This article gave me some great ideas, I can''t wait to try them out!', '2022-05-05 11:30:00', '2022-05-05 11:30:00', 12, 16),
  ('Thanks for sharing your insights on this topic, I found them very helpful', '2022-05-06 14:45:00', '2022-05-06 14:45:00', 13, 16),
  
  ('This article was really interesting, I had never thought about this topic before', '2022-05-07 08:00:00', '2022-05-07 08:00:00', 11, 17),
  ('I appreciate the practical tips and advice you provided in this article', '2022-05-08 10:30:00', '2022-05-08 10:30:00', 12, 17),
  ('Great article, I''m looking forward to incorporating some of these ideas into my daily routine', '2022-05-09 13:45:00', '2022-05-09 13:45:00', 13, 17),
  
  ('This article was really informative, I learned a lot from it', '2022-05-10 07:15:00', '2022-05-10 07:15:00', 11, 18),
  ('Thanks for sharing your insights on this topic, I found them very helpful', '2022-05-11 09:30:00', '2022-05-11 09:30:00', 12, 18),
  ('This article gave me some great ideas, I can''t wait to try them out!', '2022-05-12 12:45:00', '2022-05-12 12:45:00', 13, 18),

  ('Great article, I learned a lot from this!', '2022-05-01 10:00:00', '2022-05-01 10:00:00', 11, 19),
  ('I found this article to be very informative, thanks for sharing!', '2022-05-02 12:30:00', '2022-05-02 12:30:00', 12, 19),
  ('This article was a real eye-opener for me, I never realized how important this topic was!', '2022-05-03 15:45:00', '2022-05-03 15:45:00', 13, 19),
  
  ('I really enjoyed reading this article, it was very well-written and easy to follow', '2022-05-04 09:15:00', '2022-05-04 09:15:00', 11, 20),
  ('This article gave me some great ideas, I can''t wait to try them out!', '2022-05-05 11:30:00', '2022-05-05 11:30:00', 12, 20),
  ('Thanks for sharing your insights on this topic, I found them very helpful', '2022-05-06 14:45:00', '2022-05-06 14:45:00', 13, 20),
  
  ('This article was really interesting, I had never thought about this topic before', '2022-05-07 08:00:00', '2022-05-07 08:00:00', 11, 21),
  ('I appreciate the practical tips and advice you provided in this article', '2022-05-08 10:30:00', '2022-05-08 10:30:00', 12, 21),
  ('Great article, I''m looking forward to incorporating some of these ideas into my daily routine', '2022-05-09 13:45:00', '2022-05-09 13:45:00', 13, 21),
  
  ('This article was really informative, I learned a lot from it', '2022-05-10 07:15:00', '2022-05-10 07:15:00', 11, 22),
  ('Thanks for sharing your insights on this topic, I found them very helpful', '2022-05-11 09:30:00', '2022-05-11 09:30:00', 12, 22),
  ('This article gave me some great ideas, I can''t wait to try them out!', '2022-05-12 12:45:00', '2022-05-12 12:45:00', 13, 22),

  ('Great article, I learned a lot from this!', '2022-05-01 10:00:00', '2022-05-01 10:00:00', 11, 23),
  ('I found this article to be very informative, thanks for sharing!', '2022-05-02 12:30:00', '2022-05-02 12:30:00', 12, 23),
  ('This article was a real eye-opener for me, I never realized how important this topic was!', '2022-05-03 15:45:00', '2022-05-03 15:45:00', 13, 23),
  
  ('I really enjoyed reading this article, it was very well-written and easy to follow', '2022-05-04 09:15:00', '2022-05-04 09:15:00', 11, 24),
  ('This article gave me some great ideas, I can''t wait to try them out!', '2022-05-05 11:30:00', '2022-05-05 11:30:00', 12, 24),
  ('Thanks for sharing your insights on this topic, I found them very helpful', '2022-05-06 14:45:00', '2022-05-06 14:45:00', 13, 24),
  
  ('This article was really interesting, I had never thought about this topic before', '2022-05-07 08:00:00', '2022-05-07 08:00:00', 11, 25),
  ('I appreciate the practical tips and advice you provided in this article', '2022-05-08 10:30:00', '2022-05-08 10:30:00', 12, 25),
  ('Great article, I''m looking forward to incorporating some of these ideas into my daily routine', '2022-05-09 13:45:00', '2022-05-09 13:45:00', 13, 25);

--------------------
-- Insert Tags
--------------------
INSERT INTO "tag" (id, name)
VALUES
  (11, 'Time Management'),
  (12, 'Positive Thinking'),
  (13, 'Meditation'),
  (14, 'Procrastination'),
  (15, 'Regular Exercise'),
  (16, 'Yoga'),
  (17, 'Gratitude'),
  (18, 'Mindful Eating'),
  (19, 'Journaling'),
  (20, 'Deep Breathing'),
  (21, 'Visualization'),
  (22, 'Affirmations');

--------------------
-- Insert Article tags
--------------------
INSERT INTO "article_tag" (article_id, tag_id)
VALUES
  (11, 11),
  (12, 12),
  (13, 13),
  (14, 14),
  (15, 15),
  (16, 16),
  (17, 17),
  (18, 18),
  (19, 19),
  (20, 19),
  (21, 20),
  (22, 21),
  (23, 17),
  (24, 22),
  (25, 17),
  (25, 19);

--------------------
-- Insert Favorites
--------------------
INSERT INTO "favorite" (user_id, article_id)
VALUES
  (11, 17),
  (11, 20),
  (11, 21),
  (12, 11),
  (12, 15),
  (12, 21),
  (13, 13),
  (13, 16),
  (13, 18);

--------------------
-- Insert Followings
--------------------
INSERT INTO "follow" (followee_id, follower_id)
VALUES
  (11, 12),
  (12, 11),
  (12, 13),
  (13, 11),
  (13, 12);

--------------------
-- Update curennt value of sequences
--------------------
ALTER SEQUENCE user_id_seq RESTART WITH 20;
ALTER SEQUENCE article_id_seq RESTART WITH 30;
ALTER SEQUENCE tag_id_seq RESTART WITH 30;
