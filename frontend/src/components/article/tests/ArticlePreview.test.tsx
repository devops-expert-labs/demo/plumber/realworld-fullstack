import React from 'react'
import { render, screen } from '@testing-library/react'
import { RecoilRoot } from 'recoil'
import { mockArticle as article } from './mockData'
import ArticlePreview from '../ArticlePreview'
import { convertToDate } from '../../../utils'

jest.mock('react-router-dom', () => ({
  Link: ({ to, children }: { to: string; children: React.ReactNode }) => <a href={to}>{children}</a>,
  useNavigate: jest.fn()
}))

describe('ArticlePreview', () => {
  beforeEach(() => {
    render(<ArticlePreview article={article} />, { wrapper: RecoilRoot })
  })

  it('renders article information correctly', () => {
    expect(screen.getByText(article.title)).toBeInTheDocument()
    expect(screen.getByText(article.description)).toBeInTheDocument()
    expect(
      screen.getByRole('link', {
        name: `${article.title} ${article.description} Read more... ${article.tagList.toString().replaceAll(',', ' ')}`
      })
    ).toHaveAttribute('href', `/article/${article.slug}`)

    article.tagList.forEach((tag) => {
      expect(screen.getByText(tag)).toBeInTheDocument()
    })
    expect(screen.getByRole('link', { name: article.author.username })).toBeInTheDocument()
    expect(screen.getByText(convertToDate(article.createdAt))).toBeInTheDocument()
    expect(screen.getByRole('img')).toHaveAttribute('src', article.author.image)
  })

  it('renders favorite button correctly', () => {
    const favoriteButton = screen.getByRole('button', { name: article.favoritesCount.toString() })
    expect(favoriteButton).toBeInTheDocument()
    expect(favoriteButton).toHaveClass('btn-outline-primary')
    expect(favoriteButton).toBeEnabled()
  })
})
