import { render, screen } from '@testing-library/react'
import { RecoilRoot } from 'recoil'
import ArticleAction from '../ArticleAction'
import { mockArticle } from './mockData'

jest.mock('react-router-dom', () => ({
  useNavigate: jest.fn()
}))

describe('ArticleAction component', () => {
  const follow = jest.fn()
  const unfollow = jest.fn()
  const favorite = jest.fn()
  const unfavorite = jest.fn()
  const removeArticle = jest.fn()

  it('should render edit and delete buttons for logged in user', () => {
    render(
      <RecoilRoot>
        <ArticleAction
          isUser={true}
          article={mockArticle}
          follow={follow}
          unfollow={unfollow}
          favorite={favorite}
          unfavorite={unfavorite}
          removeArticle={removeArticle}
        />
      </RecoilRoot>
    )

    const editButton = screen.getByText('Edit Article')
    const deleteButton = screen.getByText('Delete Article')

    expect(editButton).toBeInTheDocument()
    expect(deleteButton).toBeInTheDocument()
  })

  it('should render follow button and favorite button for non-logged in user', async () => {
    render(
      <RecoilRoot>
        <ArticleAction
          isUser={false}
          article={mockArticle}
          follow={follow}
          unfollow={unfollow}
          favorite={favorite}
          unfavorite={unfavorite}
          removeArticle={removeArticle}
        />
      </RecoilRoot>
    )

    const followButton = screen.getByRole('button', { name: `Follow ${mockArticle.author.username}` })
    const favoriteButton = screen.getByRole('button', { name: `Favorite Post (${mockArticle.favoritesCount})` })

    expect(followButton).toBeInTheDocument()
    expect(favoriteButton).toBeInTheDocument()
  })
})
