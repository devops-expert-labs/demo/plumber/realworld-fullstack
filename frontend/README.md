# RealWorld React Recoil

![RealWorld React App](logo.png)

> 이 코드베이스는 완전히 동일한 **Medium.com** 클론(Conduit이라고 함)이 서로 다른 프런트엔드(React, Angular 등)와 백엔드(Node, Django 등)를 사용하여 구축하는 방법을 제공하는 [RealWorld](https://github.com/gothinkster/realworld)의 프론트엔드 프로젝트 중 하나인 [react-recoil-realworld-example-app](https://github.com/sukam09/react-recoil-realworld-example-app)를 기반으로 작성하였습니다. RealWorld는 프런트엔드와 백엔드 모두 동일한 API 사양을 준수하므로 조합하여 일치시킬 수 있습니다. 😮😎

## 기술 스택

* React 18
* TypeScript 4.7
* Recoil 0.7
* ESLint 8.2
* Prettier 2.7
* Jest 29.3
* React Testing Library 13.0

## 앱 실행

```bash
# 의존성 패키지 설치
npm install

# App 빌드 (`build` 폴더에 결과물 생성)
npm run build

# App 빌드 결과물 Clean (`build` 폴더 삭제)
npm run clean

# Lint 실행
npm run lint

# 단위 테스트 실행
npm run test

# 개발 서버 실행
npm start
```

## 주요 기능

* Home page (URL: /#/ )
  * 인기 태그 목록
  * 팔로우(Follow)한 사용자의 글 목록, 전체 글 목록, 특정 태그의 글 목록
  * 페이징(Paging)된 글 목록
* Sign in/Sign up 페이지 (URL: /#/login, /#/register )
  * JWT를 사용하여 인증(localStorage에 토큰 저장)
  * Email, Password를 사용하여 로그인
  * 계정 등록 (Username, Email, Password)
* Settings 페이지 (URL: /#/settings )
  * 프로필 이미지 URL, Username, Bio(본인 소개), Email, Password 변경
* Article 페이지 (URL: /#/article/article-slug-here )
  * 글 작성자 Follow/Unfollow 버튼
  * 글 Favourite/Unfavourite 버튼
  * 글 편집/삭제 버튼(글 작성자만 보임)
  * 서버에서 받은 Markdown 형식의 글 본문 렌더링
  * 글 내용 하단에 태그 목록
  * 페이지 하단에 댓글 작성 및 목록 영역
  * 댓글 삭제 버튼(댓글 작성자만 보임)
* Profile 페이지 (URL: /#/profile/:username, /#/profile/:username/favorites )
  * 기본 사용자 정보
  * Follow/Unfollow 버튼 또는 프로필 설정 편집 버튼(본인 프로필만 보임)
  * 사용자가 작성한 글 목록, 사용자가 Favourite한 글 목록
